"""Functionality for retrieving pipeline data for messages."""
from copy import deepcopy
from datetime import datetime
import logging

import cki_lib.misc

from . import helpers


# Beaker job search parameters
SEARCH_TABLE = 'jobsearch-0.table=Whiteboard'
SEARCH_OPERATION = 'jobsearch-0.operation=contains'
SEARCH_VALUE = 'jobsearch-0.value=cki%40gitlab%3A'


def fill_common_data(message, pipeline):
    variables = helpers.get_variables(pipeline)
    message['system'][0]['os'] = variables['cki_pipeline_branch']
    message['run']['url'] = pipeline.attributes['web_url']
    message['generated_at'] = datetime.utcnow().isoformat() + 'Z'

    return message


def fill_base_osci_data(message, pipeline):
    variables = helpers.get_variables(pipeline)

    message['artifact']['issuer'] = variables['owner']
    message['artifact']['id'] = variables['brew_task_id']

    # Remove the .src.rpm suffix from build NVR if it's present
    message['artifact']['nvr'] = variables['nvr'].replace('.src.rpm', '')

    # GitLab trigger variables don't like bools and bool('False') returns True
    if variables['is_scratch'] == 'False':
        message['artifact']['scratch'] = False
    else:
        message['artifact']['scratch'] = True

    return message


def get_gating_data(template, pipeline, project):
    messages = []
    jobs = helpers.get_newest_jobs(project, pipeline, 'test')

    for job in jobs:
        rc_data = helpers.get_rc_data(job)
        message = deepcopy(template)

        try:
            message['system'][0]['architecture'] = rc_data['kernel_arch']
        except KeyError:
            message['reason'] = 'Unable to find architecture for {}'.format(
                job.attributes['web_url']
            )
            messages.append(message)
            continue

        message['type'] = f"tier1-{message['system'][0]['architecture']}"
        if rc_data.get('debug_kernel') == 'yes':
            message['type'] += '-debug'

        # We require ALL gating tests to pass in order to mark the testing as
        # successful. This means we need to throw an error if some tests
        # couldn't be executed as that could mean that
        # (1) they crashed because of a kernel bug
        # (2) infra issue prevented them from running but if they ran, they'd
        #     fail because of a bug
        # Of course the tests could pass on rerun but the possiblility of
        # missing a kernel bug requires us to notify the maintainers.
        if rc_data['retcode'] == 0:
            message['status'] = 'passed'
        elif rc_data['retcode'] in [1, 3, 5]:
            message['status'] = 'failed'
        else:
            # Also set the status for completion
            message['status'] = 'unknown'
            message['reason'] = 'Missing test results for a required test!'

        # Add links to *all* jobs as some may have aborted.
        beaker_url = cki_lib.misc.get_env_var_or_raise('BEAKER_URL')
        pipeline_id = pipeline.attributes['id']
        full_url = '{}/jobs/?{}&{}&{}{}+{}%40{}+{}'.format(
            beaker_url,
            SEARCH_TABLE,
            SEARCH_OPERATION,
            SEARCH_VALUE,
            pipeline_id,
            rc_data['kernel_version'],
            message['system'][0]['os'],
            message['system'][0]['architecture']
        )
        message['run']['log'] = full_url
        message['run']['debug'] = full_url
        message['run']['rebuild'] = pipeline.attributes['web_url']

        messages.append(message)

    return messages


def fill_ready_for_test_data(message, message_sub_type, pipeline, project):
    variables = helpers.get_variables(pipeline)

    message['pipelineid'] = pipeline.attributes['id']
    if 'submitter' in variables:
        message['artifact']['issuer'] = variables['submitter']
    else:
        message['artifact']['issuer'] = 'CKI'  # Git build

    if message_sub_type == 'pre':
        message['cki_finished'] = False
        stage = 'setup'
        del message['status']  # Testing didn't finish yet
    else:
        message['cki_finished'] = True
        stage = 'test'

    message['patch_urls'] = variables.get('patch_urls', '').split()

    jobs = helpers.get_newest_jobs(project, pipeline, stage)
    rc_data_list = [helpers.get_rc_data(job) for job in jobs]

    message['build_info'] = []
    for index, rc_data in enumerate(rc_data_list):
        debug_kernel = True if rc_data.get('debug_kernel') == 'yes' else False

        partial_build_info = {
            'architecture': rc_data.get('kernel_arch'),
            'debug_kernel': debug_kernel,
            'kernel_package_url': rc_data.get('kernel_package_url')
        }

        # Sanity check -- do not send info about kernels that can't be
        # identified. The QE would need to drop them anyways. In theory this
        # case should never happen, in practice there is this bug we need to
        # handle: https://gitlab.com/gitlab-org/gitlab-runner/issues/4119
        if partial_build_info['architecture'] is None or \
                partial_build_info['kernel_package_url'] is None:
            logging.error('Cannot determine kernel info from job %s',
                          jobs[index].attributes['web_url'])
            continue

        # If we're sending post-test message and couldn't do any testing for
        # given kernel, skip it.
        if message['cki_finished'] and rc_data['retcode'] == 2:
            logging.error('No test results for %s',
                          jobs[index].attributes['web_url'])
            continue

        message['build_info'].append(partial_build_info)

    # Give up here if there's no kernel data when it should
    if not message['build_info']:
        logging.error('No data available for pipeline %s!',
                      pipeline.attributes['web_url'])
        message['reason'] = 'No test data found!'
        return message

    modified_files = rc_data_list[0].get('modified_files', '')
    message['modified_files'] = modified_files.split()

    message['system'][0]['stream'] = rc_data_list[0].get(
        'stream',
        variables.get('tree_name', '')
    )

    if message['cki_finished']:
        # We dropped error results from build info, we should drop them here
        # too and only summarize the status of testing that actually ran.
        all_retcodes = [rc_data['retcode'] for rc_data in rc_data_list
                        if rc_data['retcode'] != 2]
        if all([retcode == 0 for retcode in all_retcodes]):
            message['status'] = 'success'
        elif any([retcode in [1, 3, 5] for retcode in all_retcodes]):
            message['status'] = 'fail'
        else:
            logging.error('Unexpected retcodes found: %s', all_retcodes)

    return message

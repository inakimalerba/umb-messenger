import json
import logging
import os
import sys

import cki_lib.misc
import falcon
import gitlab
import sentry_sdk
from sentry_sdk.integrations.falcon import FalconIntegration

from . import helpers
from . import umb


def handle_job_event(pipeline_id, project_info, umb_config, gitlab_instance,
                     job_name=None):
    """
    Check if this event means all "setup" stages have succeeded and if so, send
    a UMB message if it was requested.
    """
    logging.debug('Handling job event from pipeline %s', pipeline_id)

    project = gitlab_instance.projects.get(project_info)
    pipeline = project.pipelines.get(pipeline_id)
    variables = helpers.get_variables(pipeline)

    if helpers.should_skip(variables):
        logging.debug('Skipping pipeline!')
        return

    # Should we send a message for this pipeline?
    send_message = variables.get('send_ready_for_test_pre', 'False')
    if send_message == 'False':
        logging.debug('Pipeline not configured for pre-test messages.')
        return

    # Check if *all* setup jobs completed and if this is the last one.
    successful_jobs = helpers.all_jobs_succeeded(
        project, pipeline, 'setup', job_name
    )
    if not successful_jobs:
        logging.debug('%s not ready for pre-test notifications.',
                      pipeline.attributes['web_url'])
        return

    umb.handle_message('ready_for_test', project, pipeline, umb_config, 'pre')


def handle_finished_pipeline(pipeline_id, project_name, umb_config,
                             gitlab_instance):
    """Send an UMB message for finished pipeline, if it was requested."""
    logging.debug('Handling any messaging for pipeline %s', pipeline_id)

    project = gitlab_instance.projects.get(project_name)
    pipeline = project.pipelines.get(pipeline_id)
    variables = helpers.get_variables(pipeline)

    if helpers.should_skip(variables):
        logging.debug('Skipping pipeline!')
        return

    # Should we send a post-test message?
    send_message = variables.get('send_ready_for_test_post', 'False')
    if send_message == 'False':
        logging.debug('Pipeline not configured for post-test messages.')
    else:
        umb.handle_message('ready_for_test',
                           project,
                           pipeline,
                           umb_config,
                           message_sub_type='post')

    # Should we send a message to OSCI?
    if 'osci' in variables.get('report_types', '').split(','):
        umb.handle_message('osci', project, pipeline, umb_config)


class GitlabWebhook:
    def __init__(self, umb_config, gitlab_instance):
        self.umb_config = umb_config
        self.gitlab = gitlab_instance

    def on_get(self, request, response):
        response.body = 'All good here!'
        response.status = falcon.HTTP_200

    def on_post(self, request, response):
        """POST request handler."""
        webhook_data = json.load(request.bounded_stream)

        if webhook_data['object_kind'] == 'build':
            # Completely skip handling messages we don't care about.
            if webhook_data['build_stage'] == 'setup' and \
                    webhook_data['build_status'] == 'success':
                # Use project_id as project_name attribute is mangled in job
                # events. path_with_namespace from pipeline event is correct
                # (cki-project/cki-pipeline) but the project_name here is
                # "CKI Project / cki-pipeline" which doesn't work well with the
                # API.
                handle_job_event(webhook_data['pipeline_id'],
                                 webhook_data['project_id'],
                                 self.umb_config,
                                 self.gitlab,
                                 webhook_data['build_name'])
        elif webhook_data['object_kind'] == 'pipeline':
            status = webhook_data['object_attributes']['status']
            if status in ['failed', 'success']:
                handle_finished_pipeline(
                    webhook_data['object_attributes']['id'],
                    webhook_data['project']['path_with_namespace'],
                    self.umb_config,
                    self.gitlab
                )
        else:
            logging.info('Unknown hook type: %s', webhook_data['object_kind'])

        response.status = falcon.HTTP_200


class ManualHook:
    def __init__(self, umb_config, gitlab_instance):
        self.umb_config = umb_config
        self.gitlab = gitlab_instance

    def on_post(self, request, response):
        """POST request handler."""
        # get pipeline data
        hook_type = request.get_param('hook_type', required=True)
        pipeline_id = int(request.get_param('id', required=True))
        project_name = request.get_param('project', required=True)
        if hook_type == 'job':
            handle_job_event(
                pipeline_id, project_name, self.umb_config, self.gitlab
            )
        elif hook_type == 'pipeline':
            handle_finished_pipeline(
                pipeline_id, project_name, self.umb_config, self.gitlab
            )
        else:
            logging.info('Unknown hook type: %s', hook_type)

        response.status = falcon.HTTP_200


def init():
    if cki_lib.misc.is_production():
        sentry_sdk.init(ca_certs=os.getenv('REQUESTS_CA_BUNDLE'),
                        integrations=[FalconIntegration()])

    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s',
                        level=logging.INFO,
                        stream=sys.stdout)
    logging.getLogger('requests').setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)

    umb_config = umb.load_configs()

    gitlab_url = cki_lib.misc.get_env_var_or_raise('GITLAB_URL')
    private_token = cki_lib.misc.get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')
    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=4)
    return (umb_config, gitlab_instance)


API = application = falcon.API()
API.req_options.auto_parse_form_urlencoded = True

config, glab = init()

webhook_receiver = GitlabWebhook(config, glab)
API.add_route('/', webhook_receiver)

manual_hook = ManualHook(config, glab)
API.add_route('/manual', manual_hook)

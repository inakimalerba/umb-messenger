"""Functionality related to UMB message sending."""
from copy import deepcopy
import json
import logging
import os
from yaml import load, Loader

import stomp

from cki_lib import misc

from . import pipeline_data
from . import templates


DEFAULT_CONFIG_PATH = '/etc/cki/umb-config/umb.yml'
DEFAULT_SSL_PEM_PATH = '/etc/cki/umb/cki-bot-prod.pem'

logging.getLogger('stomp').setLevel(logging.WARNING)


class UMBClient:
    """Class for handling UMB communication."""
    ssl_pem_path = os.environ.get('SSL_PEM_PATH') or DEFAULT_SSL_PEM_PATH

    def __init__(self, message_type, config):
        """Initialize a client to report the pipeline."""
        self.message_type = message_type
        self.config = config
        self.brokers = self.config['brokers'].items()

        self.connection = stomp.Connection(self.brokers, keepalive=True)
        self.connection.set_ssl(self.brokers,
                                key_file=self.ssl_pem_path,
                                cert_file=self.ssl_pem_path)
        self.connection.connect(wait=True)

    def send_message(self, message):
        if 'reason' in message:  # Something went wrong
            topic_config = f'{self.message_type}.error'
        else:
            topic_config = f'{self.message_type}.complete'

        try:
            topic = self.config[topic_config]
        except KeyError:
            # Not all topics may be configured, e.g. we're currently not
            # supporting error messages for 'ready_for_test'. This is a
            # feature, but we don't want to miss any real problems so log this.
            logging.info('No topic configured for %s!', topic_config)
            return

        stringified_message = json.dumps(message)

        if misc.is_production():
            logging.info('Sending message to %s', topic)
            self.connection.send(topic, stringified_message)
        else:
            logging.info('Production mode would send %s to %s', message, topic)

    def done(self):
        # Gracefully end the connection. There might be some other things we
        # want to do in the future, which is why this is in a method instead of
        # a line after message sending.
        self.connection.disconnect()


def load_configs():
    """Load UMB configuration and return a dictionary representing it."""
    config_path = os.environ.get('UMB_CONFIG_PATH') or DEFAULT_CONFIG_PATH

    with open(config_path) as config_file:
        return load(config_file, Loader=Loader)


def handle_message(
        message_type, project, pipeline, umb_config, message_sub_type=None
):
    """
    Initialize UMB client, build and send the message and end the connection.

    Args:
        message_type:     Type of the message to send. Can be 'osci' or
                          'ready_for_test'.
        project:          Project object for GitLab module.
        pipeline:         Pipeline object for gitlab module.
        umb_config:       Dictionary of UMB communication.
        message_sub_type: Optional. Can be 'pre' or 'post' for 'ready_for_test'
                          messages.
    """
    cki_branch = pipeline.attributes['ref']
    try:
        if message_type == 'osci':
            client_config = umb_config[f'report_{cki_branch}']
        else:
            client_config = umb_config['ready_for_test']
    except KeyError:
        # It's not an error if we only want to report some branches of a
        # project (the webhook is enabled per project), but we still want to
        # log this fact.
        logging.info('No UMB settings for %s (%s)!', message_type, cki_branch)
        return

    client = UMBClient(message_type, client_config)

    # Get the template, but don't overwrite it!
    message = deepcopy(getattr(templates, message_type.upper()))
    message = pipeline_data.fill_common_data(message, pipeline)

    if message_type == 'osci':
        message = pipeline_data.fill_base_osci_data(message, pipeline)
        messages = pipeline_data.get_gating_data(message, pipeline, project)
        for full_message in messages:
            client.send_message(full_message)
    else:
        message = pipeline_data.fill_ready_for_test_data(
            message, message_sub_type, pipeline, project
        )
        client.send_message(message)

    client.done()

"""Message templates."""

# Message template for ready_for_test. "None" fields mark parts that are filled
# out based on actual pipeline data.
READY_FOR_TEST = {
    'ci': {
        'name': 'CKI (Continuous Kernel Integration)',
        'team': 'CKI',
        'docs': 'https://cki-project.org',
        'url': 'https://xci32.lab.eng.rdu2.redhat.com/',
        'irc': '#kernelci',
        'email': 'cki-project@redhat.com'
    },
    'run': {
        'url': None,
    },
    'artifact': {
        'type': 'cki-build',
        'issuer': None,
        'component': 'kernel',
    },
    'system': [{
        'os': None,
        'stream': None
    }],
    'build_info': None,
    'patch_urls': None,
    'modified_files': None,
    'pipelineid': None,
    'cki_finished': None,
    'type': 'build',
    'category': 'kernel-build',
    'status': None,
    'namespace': 'cki',
    'generated_at': None,
    'version': '0.1.0'

}

# Message template for gating. "None" fields mark parts that are filled out
# based on actual pipeline data.
OSCI = {
    'ci': {
        'name': 'CKI (Continuous Kernel Integration)',
        'team': 'CKI',
        'docs': 'https://cki-project.org',
        'url': 'https://xci32.lab.eng.rdu2.redhat.com/',
        'irc': '#kernelci',
        'email': 'cki-project@redhat.com'
    },
    'run': {
        'url': None,
        'log': None,
        'debug': None,
        'rebuild': None
    },
    'artifact': {
        'type': 'brew-build',
        'id': None,
        'issuer': None,
        'component': 'kernel',
        'nvr': None,
        'scratch': None
    },
    'system': [{
        'os': None,
        'provider': 'beaker',
        'architecture': None
    }],
    'type': None,
    'category': 'functional',
    'status': None,
    'namespace': 'cki',
    'generated_at': None,
    'version': '0.1.0'
}

# umb-messenger

Webhook responsible for UMB message sending. Messages can be sent upon
completion of the specified stage or finished pipelines.

## Currently supported messages

* **OSCI:** For details about schema and use case, check out [Fedora CI messages].
  Messages are sent upon pipeline completion, if the pipeline was configured to
  support the messages.

* **Ready for test:** Messages used to trigger testing in specialized labs.
  Meant to be used for testing that is not feasible to onboard into CKI (e.g.
  requires controlled environment or special hardware). A message is sent after
  a kernel is built *and* after CKI functional testing completes.

### Ready for test message schema

```
{
    "ci": {
      "name": "CKI (Continuous Kernel Integration)",
      "team": "CKI",
      "docs": "https://cki-project.org/",
      "url": "https://xci32.lab.eng.rdu2.redhat.com/",
      "email": "cki-project@redhat.com",
      "irc": "#kernelci"
    },
    "run": {
      "url": "<PIPELINE_URL>"
    },
    "artifact": {
      "type": "cki-build",
      "issuer": "<PATCH_AUTHOR_EMAIL or CKI>",
      "component": "kernel"
    },
    "system": [
      {
        "os": "<CKI_PIPELINE_BRANCH>",
        "stream": "<STREAM>"
      }
    ],
    "build_info": [
      {
        "architecture": "<ARCH>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      {
        "architecture": "<ARCH>",
        "kernel_package_url": "<LINK_TO_REPO>",
        "debug_kernel": bool
      },
      ...
    ],
    "patch_urls": ["list", "of", "strings", "or", "empty", "list"],
    "modified_files": ["list", "of", "strings", "or", "empty", "list"],
    "pipelineid": <ID>,
    "cki_finished": bool,
    "status": <"success" or "fail">,  # attribute NOT present if cki_finished is false
    "category": "kernel-build",
    "namespace": "cki",
    "type": "build",
    "generated_at": "<DATETIME_STRING>",
    "version": "0.1.0"
}
```

`CKI_PIPELINE_BRANCH` matches major OS release for RHEL, e.g. `rhel7` or
`rhel8`. All upstream kernels are being tested on a current Fedora release and
the value matches the tested tree, e.g. `scsi` or `upstream-stable`. `STREAM`
further specifies the RHEL stream used. The syntax matches mapping from
[select stream script](https://gitlab.com/cki-project/pipeline-definition/blob/master/select_stream.py).
If stream can't be distinguished (e.g. in case of upstream kernels), the value
matches `tree_name` variable.

### Ready for test details

Messages are being sent to `/topic/VirtualTopic.eng.cki.ready_for_test`. All
labs involved in extra testing will be notified if the topic is changed. The
results should arrive back to `/topic/VirtualTopic.eng.cki.results`, description
of the messages can be found in [receiver's documentation].

Onboarded labs can filter specific messages they are interested in. Some
examples include:

* Filtering on modified files to only test changes to their subsystem
* Filtering on `system/os` to only test changes to specific release
* Filtering on `cki_finished == false` to run testing in parallel
* Filtering on `cki_finished == true && status == 'success'` to e.g. only run
  performance tests if CKI functional testing passed
* Filtering on `artifact/issuer != 'CKI'` to only test proposed changes instead
  of already merged ones
* Combination of above

## How to run this project

The messenger can be deployed locally by running

```
gunicorn --bind 0.0.0.0 --reload -k sync umb_messenger.webhook --timeout 120 --workers 5
```

Note that you need the appropriate environment variables exposed and have UMB
certificate and configuration in the right place!

Required environment variables:

* `GITLAB_URL`
* `GITLAB_PRIVATE_TOKEN`

By default, the UMB certificate lives in `/etc/cki/umb/cki-bot-prod.pem`. This
value can be overriden by exposing a `DEFAULT_SSL_PEM_PATH` variable. UMB
configuration is expected to be in `/etc/cki/umb-config/umb.yml` and can be
overriden by `DEFAULT_CONFIG_PATH` variable.

### Manual triggering

Messaging can be manually triggered by `http` command:

```
http -f POST <url>/manual hook_type=<type> project=<project_name> id=<id>
```

* `hook_type` can be `job` or `pipeline`. The naming corresponds to mocked
  GitLab webhook. For parallel testing, use `job`. For post-testing messages
  (e.g. OSCI), use `pipeline`.
* `project` is a full project name (including the namespace) where the pipeline
   was executed.
* `id` is the pipeline ID.

### Setting up the webhook

First, have the project deployed and running. You can use a container from this
repository and add the UMB configuration, certificate and expose required
variables. Then you can start it by running the `gunicorn` command from above.
The command can be put into container's entrypoint if you wish to set up
automatic deployments.

After you verify everything is running correctly, go to your GitLab project's
`settings/integration` page. Point the `URL` to the webhook, tick `Job events`
and `Pipeline events` (make sure all other fields are not picked!) and click on
`Add webhook`. The webhook should start receiving the new events.

### Production setup

If `IS_PRODUCTION` is set to `true`, [sentry] support is enabled via
`SENTRY_DSN`. You can the value in your sentry project's settings under
`SDK Setup / Client Keys`.


[Fedora CI messages]: https://pagure.io/fedora-ci/messages/
[receiver's documentation]: https://gitlab.com/cki-project/pipeline-trigger/#umb-result-message-format
[sentry]: https://sentry.io/welcome/

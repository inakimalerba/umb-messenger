"""Tests for umb_messenger.helpers."""

import unittest

import umb_messenger.helpers as helpers

import fakes


class TestAllJobsSucceeded(unittest.TestCase):
    """Tests for helpers.all_jobs_succeeded()."""
    def test_unsuccessful(self):
        """Verify empty list is returned if there are unsuccessful jobs."""
        job_attrs = {'stage': 'valid', 'status': 'failed', 'name': 'no',
                     'finished_at': 'yesterday'}

        project = fakes.FakeGitLabProject()
        project.pipelines.add_new_pipeline('failed')
        pipe = project.pipelines.get(0)
        pipe.jobs.add_job(1, job_attrs)
        project.jobs.add_job(1, job_attrs)

        self.assertEqual(
            helpers.all_jobs_succeeded(project, pipe, 'valid', None),
            []
        )

    def test_successful(self):
        """Verify nonempty list is returned if all jobs are successful."""
        project = fakes.FakeGitLabProject()
        project.pipelines.add_new_pipeline('failed')
        pipe = project.pipelines.get(0)
        for obj in [project, pipe]:
            obj.jobs.add_job(
                1, {'stage': 'valid', 'status': 'success', 'name': 'no',
                    'finished_at': '2020-02-23'}
            )
            obj.jobs.add_job(
                2, {'stage': 'valid', 'status': 'success', 'name': 'yes',
                    'finished_at': '2020-02-23'}
            )
            obj.jobs.add_job(
                3, {'stage': 'different', 'status': 'failed', 'name': 'yes',
                    'finished_at': '2020-02-24'}
            )

        self.assertEqual(
            len(helpers.all_jobs_succeeded(project, pipe, 'valid', None)),
            2
        )

    def test_override(self):
        """
        Verify retries are considered if multiple jobs with the same name
        exist.
        """
        project = fakes.FakeGitLabProject()
        project.pipelines.add_new_pipeline('success')
        pipe = project.pipelines.get(0)
        for obj in [project, pipe]:
            obj.jobs.add_job(
                1, {'stage': 'valid', 'status': 'fail', 'name': 'yes',
                    'finished_at': '2020-02-24'}
            )
            obj.jobs.add_job(
                2, {'stage': 'valid', 'status': 'success', 'name': 'yes',
                    'finished_at': '2020-02-23'}
            )
        self.assertEqual(
            len(helpers.all_jobs_succeeded(project, pipe, 'valid', None)),
            1
        )

    def test_last_finished(self):
        """Verify last finished job comparison."""
        project = fakes.FakeGitLabProject()
        project.pipelines.add_new_pipeline('success')
        pipe = project.pipelines.get(0)
        for obj in [project, pipe]:
            obj.jobs.add_job(
                1, {'stage': 'valid', 'status': 'fail', 'name': 'yes',
                    'finished_at': '2020-02-24'}
            )
            obj.jobs.add_job(
                2, {'stage': 'valid', 'status': 'success', 'name': 'yes',
                    'finished_at': '2020-02-23'}
            )

        self.assertEqual(
            len(helpers.all_jobs_succeeded(project, pipe, 'valid', 'yes')),
            1
        )

        # Add another job from the stage that finished later
        for obj in [project, pipe]:
            obj.jobs.add_job(
                3, {'stage': 'valid', 'status': 'success', 'name': 'no',
                    'finished_at': '2020-02-24'}
            )
        self.assertEqual(
            len(helpers.all_jobs_succeeded(project, pipe, 'valid', 'yes')),
            0
        )


class TestGetVariables(unittest.TestCase):
    """Tests for helpers.get_variables()."""
    def test_variables(self):
        """Sanity check for variable-to-dict transformation."""
        variables = {'var1': 'val1', 'var2': 'val2'}
        pipe = fakes.FakePipeline('failed', 1, variables)

        self.assertEqual(variables, helpers.get_variables(pipe))


class TestRCData(unittest.TestCase):
    """Tests for helpers.get_rc_data()."""
    def test_rc_data(self):
        """
        Sanity check for state data retrieval. Verifies cki-lib's functionality
        didn't change unexpectedly.
        """
        fake_job = fakes.FakeProjectJob(1, 1, {})
        data = b'[state]\ninfo = yes\nmore = no\n\n[new]\nstate = false\n'
        fake_job._artifact_data['rc'] = data

        state_data = {'info': 'yes', 'more': 'no'}

        self.assertEqual(state_data, helpers.get_rc_data(fake_job))


class TestGetNewestJobs(unittest.TestCase):
    """Tests for helpers.get_newest_jobs()."""
    def test_unfinished_jobs(self):
        """Verify empty list is returned if there are unfinished jobs."""
        pipe = fakes.FakePipeline('running', 1)
        pipe.jobs.add_job(1, {'stage': 'ok', 'finished_at': 'yesterday'})
        pipe.jobs.add_job(2, {'stage': 'ok', 'finished_at': None})  # retry

        self.assertEqual([], helpers.get_newest_jobs(None, pipe, 'ok'))

    def test_retry_filtering(self):
        """
        Verify retried jobs are filtered out if there are multiple ones with
        the same name.
        """
        project = fakes.FakeGitLabProject()
        pipe = fakes.FakePipeline('running', 1)

        jobs = [{'stage': 'ok',
                 'finished_at': '2019-08-05T07:31:12.173Z',
                 'name': 'same'},
                {'stage': 'different',
                 'finished_at': '2019-08-05T07:31:12.173Z',
                 'name': 'different as well'},
                {'stage': 'ok',
                 'finished_at': '2019-08-05T07:31:15.173Z',
                 'name': 'same'}]

        for index, job in enumerate(jobs):
            pipe.jobs.add_job(index, job)
            project.jobs.add_job(index, job)

        newest_jobs = helpers.get_newest_jobs(project, pipe, 'ok')
        self.assertEqual(len(newest_jobs), 1)
        self.assertEqual(newest_jobs[0].attributes['id'], 2)


class TestShouldSkip(unittest.TestCase):
    """Tests for helpers.should_skip()."""
    def test_sanity(self):
        """Sanity check for correctly skipped pipelines."""
        # Test not skipping
        variables = {'retrigger': 'false'}
        self.assertFalse(helpers.should_skip(variables))

        # Test retrigger
        variables = {'retrigger': 'True'}
        self.assertTrue(helpers.should_skip(variables))

        # Test kickstart
        variables = {'title': 'Patch kickstart'}
        self.assertTrue(helpers.should_skip(variables))

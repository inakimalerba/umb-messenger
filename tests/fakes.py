"""Fake classes."""

import unittest.mock as mock


class FakePipelineJob():
    def __init__(self, job_id, index, attributes):
        self.attributes = {'id': job_id,
                           'web_url': f'joburl/{index}'}
        for key, value in attributes.items():
            self.attributes[key] = value


class FakePipelineJobs:
    def __init__(self):
        self._jobs = []

    def add_job(self, job_id, attributes):
        self._jobs.append(FakePipelineJob(job_id, len(self._jobs), attributes))

    def list(self, **kwargs):
        return self._jobs


class FakeVariable:
    def __init__(self, key, value):
        self.key = key
        self.value = value


class FakePipelineVariables:
    def __init__(self, variables):
        self._vars = []
        if variables:  # Handle None
            for key, value in variables.items():
                self._vars.append(FakeVariable(key, value))

    def list(self, **kwargs):
        return self._vars


class FakePipeline:
    def __init__(self, status, index, variables=None):
        self.jobs = FakePipelineJobs()
        self.attributes = {'status': status,
                           'web_url': f'pipelineurl/{index}',
                           'id': index}
        self.variables = FakePipelineVariables(variables)


class FakeProjectJob():
    def __init__(self, job_id, index, attributes):
        self.attributes = {'id': job_id,
                           'web_url': f'joburl/{index}'}
        for key, value in attributes.items():
            self.attributes[key] = value

        self._artifact_data = {}

    def artifact(self, name):
        return self._artifact_data.get(name)


class FakeProjectJobs:
    def __init__(self):
        self._jobs = []

    def add_job(self, job_id, attributes):
        self._jobs.append(FakeProjectJob(job_id, len(self._jobs), attributes))

    def list(self, **kwargs):
        return self._jobs

    def get(self, job_id):
        for job in self._jobs:
            if job.attributes['id'] == job_id:
                return job

        return None


class FakeProjectPipelines:
    def __init__(self):
        self._pipelines = []

    def add_new_pipeline(self, status, variables=None):
        self._pipelines.append(
            FakePipeline(status, len(self._pipelines), variables)
        )

    def get(self, index):
        return self._pipelines[index]

    def list(self, **kwargs):
        return self._pipelines


class FakeGitLabProject:
    def __init__(self):
        self.manager = mock.MagicMock()
        self.pipelines = FakeProjectPipelines()
        self.jobs = FakeProjectJobs()


class FakeGitLab:
    def __init__(self):
        self.projects = {}

    def add_project(self, project_name):
        self.projects[project_name] = FakeGitLabProject()

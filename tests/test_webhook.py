"""
Tests for umb_messenger.webhook.

Note that the webhook file is checking environment variables right upon import,
which is why we have to import it after the mocks are active. Class mocks are
not available yet in setUp so we can't import the webhook in there and instead,
have to do it in every function. If anyone has a better way to solve the
problem, patches welcome.
"""
import json
import unittest
import unittest.mock as mock

import falcon.testing

import fakes


@mock.patch('cki_lib.misc.get_env_var_or_raise')
@mock.patch('umb_messenger.umb.load_configs')
class TestHandleJobEvent(unittest.TestCase):
    """Tests for webhook.handle_job_event()."""
    @mock.patch('umb_messenger.helpers.all_jobs_succeeded')
    @mock.patch('umb_messenger.umb.handle_message')
    def test_message(self, mock_send, mock_success, mock_configs, mock_env):
        """
        Verify all functions are called with expected arguments if we're
        supposed to send a message.
        """
        from umb_messenger.webhook import handle_job_event

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'send_ready_for_test_pre': 'True'}
        )
        pipe = fake_gitlab.projects.get('project').pipelines.get(0)

        handle_job_event(0, 'project', {}, fake_gitlab)

        mock_success.assert_called_once_with(
            fake_gitlab.projects.get('project'), pipe, 'setup', None
        )
        mock_send.assert_called_once_with(
            'ready_for_test', mock.ANY, pipe, {}, 'pre'
        )

    def test_no_pre_test(self, mock_configs, mock_env):
        """Verify pre-test mesages are not sent if not configured."""
        from umb_messenger.webhook import handle_job_event

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'fail', {}
        )

        with self.assertLogs(level='DEBUG') as log:
            handle_job_event(0, 'project', {}, fake_gitlab)
            self.assertIn('not configured for pre-test', log.output[-1])

    @mock.patch('umb_messenger.helpers.all_jobs_succeeded')
    def test_failed(self, mock_success, mock_configs, mock_env):
        """Verify pre-test mesages are not sent if setup jobs failed."""
        from umb_messenger.webhook import handle_job_event

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'fail', {'send_ready_for_test_pre': 'True'}
        )
        mock_success.return_value = []

        with self.assertLogs(level='DEBUG') as log:
            handle_job_event(0, 'project', {}, fake_gitlab)
            self.assertIn('not ready for pre-test notifications',
                          log.output[-1])


@mock.patch('cki_lib.misc.get_env_var_or_raise')
@mock.patch('umb_messenger.umb.load_configs')
class TestHandleFinished(unittest.TestCase):
    """Tests for webhook.handle_finished_pipeline()."""
    @mock.patch('umb_messenger.umb.handle_message')
    def test_osci_message(self, mock_send, mock_configs, mock_env):
        """
        Verify all functions are called if we're supposed to send an OSCI
        message.
        """
        from umb_messenger.webhook import handle_finished_pipeline

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'report_types': 'email,osci'}
        )

        with self.assertLogs(level='DEBUG') as log:
            handle_finished_pipeline(0, 'project', {}, fake_gitlab)
            self.assertIn('not configured for post-test', log.output[-1])

        mock_send.assert_called_once()

    @mock.patch('umb_messenger.umb.handle_message')
    def test_post_test_message(self, mock_send, mock_configs, mock_env):
        """
        Verify all functions are called if we're supposed to send a post-test
        message.
        """
        from umb_messenger.webhook import handle_finished_pipeline

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline(
            'success', {'send_ready_for_test_post': 'True'}
        )

        handle_finished_pipeline(0, 'project', {}, fake_gitlab)
        mock_send.assert_called_once()

    @mock.patch('umb_messenger.umb.handle_message')
    def test_no_message(self, mock_send, mock_configs, mock_env):
        """Verify we don't send any messages when we're not supposed to."""
        from umb_messenger.webhook import handle_finished_pipeline

        fake_gitlab = fakes.FakeGitLab()
        fake_gitlab.add_project('project')
        fake_gitlab.projects['project'].pipelines.add_new_pipeline('success')

        handle_finished_pipeline(0, 'project', {}, fake_gitlab)
        mock_send.assert_not_called()


@mock.patch('cki_lib.misc.get_env_var_or_raise')
@mock.patch('umb_messenger.umb.load_configs')
class TestManualHook(falcon.testing.TestCase):
    """Tests for webhook.ManualHook.on_post()."""
    def test_bad_data(self, mock_configs, mock_env):
        """
        Verify we don't do anything if we are missing parameters or get an
        unknown hook type.
        """
        import umb_messenger.webhook
        self.app = umb_messenger.webhook.application

        params = {'hook_type': 'bad-one', 'id': 11}
        response = self.simulate_post('/manual', params=params)
        self.assertIn('Missing parameter', response.json['title'])

        params = {'hook_type': 'bad-one', 'id': 11, 'project': 'thisone'}
        with self.assertLogs(level='INFO') as log:
            response = self.simulate_post('/manual', params=params)
            self.assertIn('Unknown hook type', log.output[-1])

    def test_job_call(self, mock_configs, mock_env):
        """Verify handle_job_event is called with the correct parameters."""
        import umb_messenger.webhook
        self.app = umb_messenger.webhook.application

        params = {'hook_type': 'job', 'id': 11, 'project': 'thisone'}
        with mock.patch('umb_messenger.webhook.handle_job_event') as mock_job:
            self.simulate_post('/manual', params=params)
            mock_job.assert_called_once_with(11, 'thisone', mock.ANY, mock.ANY)

    def test_pipeline_call(self, mock_configs, mock_env):
        """
        Verify handle_finished_pipeline is called with the correct parameters.
        """
        import umb_messenger.webhook
        self.app = umb_messenger.webhook.application

        params = {'hook_type': 'pipeline', 'id': 11, 'project': 'thisone'}
        with mock.patch('umb_messenger.webhook.handle_finished_pipeline') as mock_pipe:  # noqa
            self.simulate_post('/manual', params=params)
            mock_pipe.assert_called_once_with(
                11, 'thisone', mock.ANY, mock.ANY
            )


@mock.patch('cki_lib.misc.get_env_var_or_raise')
@mock.patch('umb_messenger.umb.load_configs')
class TestWebhook(falcon.testing.TestCase):
    """Tests for webhook.GitlabWebhook - on_post and on_get methods."""
    def test_bad_data(self, mock_configs, mock_env):
        """Verify we don't do anything if we get an unknown hook type."""
        import umb_messenger.webhook
        self.app = umb_messenger.webhook.application

        params = {'object_kind': 'unknown'}
        with self.assertLogs(level='INFO') as log:
            response = self.simulate_post('/', body=json.dumps(params))
            self.assertIn('Unknown hook type', log.output[-1])

    def test_job_call(self, mock_configs, mock_env):
        """
        Verify handle_job_event is called with the correct parameters and
        skipped when it should be.
        """
        import umb_messenger.webhook
        self.app = umb_messenger.webhook.application

        params = {'object_kind': 'build',
                  'build_stage': 'setup',
                  'build_name': 'setup x86_64',
                  'build_status': 'success',
                  'pipeline_id': 11,
                  'project_id': 3}
        with mock.patch('umb_messenger.webhook.handle_job_event') as mock_job:
            self.simulate_post('/', body=json.dumps(params))
            mock_job.assert_called_once_with(
                11, 3, mock.ANY, mock.ANY, 'setup x86_64'
            )

            mock_job.reset_mock()
            params['build_stage'] = 'test'
            self.simulate_post('/', body=json.dumps(params))
            mock_job.assert_not_called()

    def test_pipeline_call(self, mock_configs, mock_env):
        """
        Verify handle_finished_pipeline is called with the correct parameters
        and skipped when it should be.
        """
        import umb_messenger.webhook
        self.app = umb_messenger.webhook.application

        params = {'object_kind': 'pipeline',
                  'object_attributes': {'status': 'failed', 'id': 15},
                  'project': {'path_with_namespace': 'thisone'}}
        with mock.patch('umb_messenger.webhook.handle_finished_pipeline') as mock_pipe:  # noqa
            self.simulate_post('/', body=json.dumps(params))
            mock_pipe.assert_called_once_with(
                15, 'thisone', mock.ANY, mock.ANY
            )

            mock_pipe.reset_mock()
            params['object_attributes']['status'] = 'running'
            self.simulate_post('/', body=json.dumps(params))
            mock_pipe.assert_not_called()

    def test_get(self, mock_configs, mock_env):
        """Check GET calls return 200 if the hook is running."""
        import umb_messenger.webhook
        self.app = umb_messenger.webhook.application

        response = self.simulate_get('/')
        self.assertEqual(response.status, '200 OK')

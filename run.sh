#!/bin/bash

gunicorn --bind 0.0.0.0 --reload -k sync umb_messenger.webhook --timeout 120 --workers 5 2>&1 | \
    tee -a "/logs/umb-messenger.log"
